FROM gentoo/portage:latest as portage
FROM gentoo/stage3:latest

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

ENV ACCEPT_KEYWORDS="~amd64" \
    EMERGE_DEFAULT_OPTS="--jobs=4 --getbinpkg" \
    FEATURES="-sandbox -usersandbox -ipc-sandbox -mount-sandbox -network-sandbox -pid-sandbox binpkg-request-signature" \
    MAKEOPTS="-j4" \
    USE="-perl"

RUN emerge dev-vcs/git dev-util/pkgcheck \
 && emerge --info dev-util/pkgcheck \
 && rm -rf /var/cache/distfiles/*
