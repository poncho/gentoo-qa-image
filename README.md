# gentoo-qa-image

To check your overlay for QA issues, add the following `.gitlab-ci.yml`:

```yaml
image: $CI_REGISTRY/poncho/gentoo-qa-image:latest

stages:
  - pkgcheck

variables:
  GIT_DEPTH: 1

pkgcheck:
  stage: pkgcheck
  script:
    - pkgcheck --version
    - pkgcheck scan --exit
```
